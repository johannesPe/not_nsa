import glob
import cv2
import numpy as np
from matplotlib import pyplot as plt

def find_image_names(directory):
    if directory[-1]!='/':
        directory = directory + '/'
    all_files = glob.glob(directory+"*")
    prefixes = []
    for filename in file_list:
        name = filename.split('/')[1]
        prefix = name.split('_')[0]
        prefixes.append(prefix)
    return set(prefixes)

def load_images(prefix,directory='./'):
    images = []
    for i in range(1,6):
        filename = directory+prefix+'_'+str(i)+'.jpeg'
        img = cv2.imread(filename)
        images.append(img)
    return images


def drawMatches(img1, kp1, img2, kp2, matches):
    """
    My own implementation of cv2.drawMatches as OpenCV 2.4.9
    does not have this function available but it's supported in
    OpenCV 3.0.0

    This function takes in two images with their associated
    keypoints, as well as a list of DMatch data structure (matches)
    that contains which keypoints matched in which images.

    An image will be produced where a montage is shown with
    the first image followed by the second image beside it.

    Keypoints are delineated with circles, while lines are connected
    between matching keypoints.

    img1,img2 - Grayscale images
    kp1,kp2 - Detected list of keypoints through any of the OpenCV keypoint
              detection algorithms
    matches - A list of matches of corresponding keypoints through any
              OpenCV keypoint matching algorithm
    """

    # Create a new output image that concatenates the two images together
    # (a.k.a) a montage
    rows1 = img1.shape[0]
    cols1 = img1.shape[1]
    rows2 = img2.shape[0]
    cols2 = img2.shape[1]

    out = np.zeros((max([rows1,rows2]),cols1+cols2,3), dtype='uint8')

    # Place the first image to the left
    out[:rows1,:cols1] = np.dstack([img1, img1, img1])

    # Place the next image to the right of it
    out[:rows2,cols1:] = np.dstack([img2, img2, img2])

    # For each pair of points we have between both images
    # draw circles, then connect a line between them
    for mat in matches:

        # Get the matching keypoints for each of the images
        img1_idx = mat.queryIdx
        img2_idx = mat.trainIdx

        # x - columns
        # y - rows
        (x1,y1) = kp1[img1_idx].pt
        (x2,y2) = kp2[img2_idx].pt

        # Draw a small circle at both co-ordinates
        # radius 4
        # colour blue
        # thickness = 1
        cv2.circle(out, (int(x1),int(y1)), 4, (255, 0, 0), 1)
        cv2.circle(out, (int(x2)+cols1,int(y2)), 4, (255, 0, 0), 1)

        # Draw a line in between the two points
        # thickness = 1
        # colour blue
        cv2.line(out, (int(x1),int(y1)), (int(x2)+cols1,int(y2)), (255, 0, 0), 1)


    # Show the image
    #cv2.imshow('Matched Features', out)
    #cv2.waitKey(0)
    #cv2.destroyWindow('Matched Features')

    # Also return the image if you'd like a copy
    return out

def align_images(query_img, source_img):
    MIN_MATCH_COUNT = 10
    #maybe crop the query image so it can be found easier?
    img1 = cv2.imread(query_img,0)          # queryImage
    img2 = cv2.imread(source_img,0) # trainImage

    img1 = cv2.resize(img1, (310, 233))
    img2 = cv2.resize(img2, (310, 233))

    # Initiate SIFT detector
    sift = cv2.SIFT()

    # find the keypoints and descriptors with SIFT
    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(des1,des2,k=2)

    # store all the good matches as per Lowe's ratio test.
    good = []
    for m,n in matches:
        if m.distance < 0.7*n.distance:
            good.append(m)

    print img2.shape
    if len(good)>MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
        matchesMask = mask.ravel().tolist()

        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)

        cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.CV_AA)

    else:
        print "Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT)
        matchesMask = None

    draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)


#translate image
M = np.float32([[1,0,-dst[1,0,0]+10],[0,1,0]])
img2_trans = cv2.warpAffine(img2,M,(cols,rows))

#rotate image
rows,cols = img2.shape
angle = -get_rot_angle(dst)
M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
img2_rot = cv2.warpAffine(img2_trans,M,(cols,rows))
plt.imshow(img2_trans, 'gray'),plt.show()

    img3 = drawMatches(img1=img1,kp1=kp1,img2=img2,kp2=kp2,matches=good)
    plt.imshow(img3, 'gray'),plt.show()

def get_rot_angle(dst):
    vec1 = dst[1,0] - dst[0,0]
    vec2 = np.asarray([0,1])
    return np.arccos(np.inner(vec1,vec2) / (np.sqrt(np.sum(vec1**2)) * np.sqrt(np.sum(vec2**2))))

from cv2 import cv
def subimage(image, dst):
    height = int(np.sqrt(np.sum((dst[1,0] - dst[0,0])**2)))
    width = int(np.sqrt(np.sum((dst[2,0] - dst[1,0])**2)))
    theta = np.rad2deg(get_rot_angle(dst))
    centre = dst[0,0] + (dst[1,0] - dst[0,0])/2 + (dst[3,0] - dst[0,0])/2
    output_image = cv.CreateImage((width, height), image.depth, image.nChannels)
    mapping = np.array([[np.cos(theta), -np.sin(theta), centre[0]],
                       [np.sin(theta), np.cos(theta), centre[1]]])
    map_matrix_cv = cv.fromarray(mapping)
    cv.GetQuadrangleSubPix(image, output_image, map_matrix_cv)
    output_image=np.asarray(cv.GetMat(output_image))
    return output_image

if __name__=="__main__":
    align_images(query_img='test_sm/set2_2.jpeg', source_img='test_sm/set2_1.jpeg')
